document.addEventListener('deviceready', onDeviceReady, false);

const TEXTO_PANTALLA_PRINCIPAL = "Tocar la pantalla para comenzar a escanear.";
const TEXTO_PANTALLA_SCANNER = "Tocar de nuevo para volver a la pantalla principal";

function restablecerPantallaPrincipal() {
  QRScanner.destroy(function(status){
    console.log(status);
  });

  document.getElementById('textoExplicativo').innerText = TEXTO_PANTALLA_PRINCIPAL;

  let appNavigator = document.getElementById('appNavigator');
  for (i in appNavigator.pages) {
    appNavigator.pages[i].getElementsByClassName('page__background')[0].classList.remove('fondoTransparente');
  }

  let elementos = document.getElementsByClassName('transparentar');
  for (let i=0; i < elementos.length; i++) {
    elementos[i].classList.remove('fondoTransparente');
  }

  document.getElementById('tabbarPage').onclick = mostrarScanner;
}

function completarDatosEstudianteEnPantallaDDJJ(estudiante) {
  for (campo in estudiante) {
    if (campo != "id") {
      document.querySelector(`#${campo}`).innerText = estudiante[campo];
    }
  }
}

function completarDatosDDJJenPantallaDDJJ(ddjj) {
  for (campo in ddjj) {
    if (campo != "id") {
      if (esSintoma(campo)) {
        document.querySelector(`#${campo}`).innerText = (ddjj[campo] == 0 ? "No" : "Sí");
      } else {
        document.querySelector(`#${campo}`).innerText = ddjj[campo];
      }
    }
  }
}

function cargarPantallaDDJJ(qr) {
  completarDatosEstudianteEnPantallaDDJJ(qr.estudiante);
  completarDatosDDJJenPantallaDDJJ(qr.ddjj)
}

function mostrarDDJJ(qr) {
  QRScanner.destroy(function(status){
    console.log(status);
  });
  document.querySelector('#appNavigator')
  .pushPage('ddjj.html', {data: qr})
  .then(function() {
    cargarPantallaDDJJ(qr);
  });
}

function esSintoma(campo) {
  return (
    campo != "id" && 
    campo != "fecha" && 
    campo != "temperatura" && 
    campo != "nombre_rp" && 
    campo != "dni_rp" &&
    campo != "estudiante_id" 
  );
}

function haySintomasEnDDJJ(qr) {
  if (qr.ddjj.temperatura > 37) {
    return true;
  }

  for (campo in qr.ddjj) {
    if (esSintoma(campo) && qr.ddjj[campo]) {
      return true;
    }
  }
}

function mostrarOK(qr) {
  enviarIngresoPermitidoAlBackoffice(qr);
  let notificacion = `${qr.estudiante.nombre} ${qr.estudiante.apellido}
  
Curso: ${qr.estudiante.curso}

OK ✅`;

  alert (notificacion);
}

function mostrarAlertaPorSíntomas(qr) {
  let notificacion = `ATENCIÓN

Estudiante ${qr.estudiante.nombre} ${qr.estudiante.apellido} (Curso: ${qr.estudiante.curso}) declara síntomas.

Verificar declaración jurada.`;

  alert(notificacion);
  mostrarDDJJ(qr);
}

function mostrarAlertaPorFecha(qr) {
  let notificacion = `ATENCIÓN

DECLARACION JURADA VENCIDA

Estudiante ${qr.estudiante.nombre} ${qr.estudiante.apellido} (Curso: ${qr.estudiante.curso})`;

  alert(notificacion);
}

function declaraciónJuradaVigente(qr) {
  const fechaActual = new Date();
  fechaActual.setHours(0,0,0,0);

  let fechaDDJJ = new Date(qr.ddjj.fecha);

  const dias = (fechaActual - fechaDDJJ) / (1000*3600*24);

  return (dias >= 0 && dias < 2);
}

function analizarDDJJ(qr) {
  if (!declaraciónJuradaVigente(qr)) {
    mostrarAlertaPorFecha(qr);
  } else if (haySintomasEnDDJJ(qr)) {
    mostrarAlertaPorSíntomas(qr);
    // mostrarDDJJ(qr); // Reemplazar esto por código con soporte para ejecución asincrónica.
  } else {
    mostrarOK(qr);
  }
}

function procesarQR(err, text){
  if(err) {
      console.error(err);
  } else {
      analizarDDJJ(JSON.parse(text)); 
  }
  // restablecerPantallaPrincipal(); // Descomentar para que vuelva a la pantalla principal luego de scanear.
  QRScanner.scan(procesarQR);
}

function ocultarDDJJ() {
  document.querySelector('#appNavigator')
  .popPage()
  .then(function() {
    mostrarScanner();
  });
}

function mostrarScanner() {
  QRScanner.show();
  QRScanner.scan(procesarQR);

  document.getElementById('textoExplicativo').innerText = TEXTO_PANTALLA_SCANNER;
  let appNavigator = document.getElementById('appNavigator');
  for (i in appNavigator.pages) {
    appNavigator.pages[i].getElementsByClassName('page__background')[0].classList.add('fondoTransparente');
  }

  let elementos = document.getElementsByClassName('transparentar');
  for (let i=0; i < elementos.length; i++) {
    elementos[i].classList.add('fondoTransparente');
  }

  document.getElementById('tabbarPage').onclick = restablecerPantallaPrincipal;
}

function onDeviceReady() {
    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
}

function enviarIngresoAlBackoffice(qr, estado) {
  console.log("enviarIngresoAlBackoffice")
  const server = "http://192.168.0.178:8000"
  qr["ingreso"] = { "estado": estado };
  const request = new XMLHttpRequest();
  request.open("POST", server + "/api/ingresos/", true);
  request.setRequestHeader("content-type", "application/json;charset=utf-8");
  request.onreadystatechange = function(){
    if (request.readyState==4)  {
      if(request.status==201){
        console.log(request.responseText);
      }else {
        console.log(request.status);
        console.log(request.responseText);
      }
    }
  };
  request.onerror = function(e){
    console.group("Error en request");
    console.error(`request.readyState: ${request.readyState}`);
    console.error(`request.status: ${request.status}`);
    console.error(`request.statusText: ${request.statusText}`);
    console.error(`request.responseText: ${request.responseText}`);
    console.groupEnd();
  };

  request.send(JSON.stringify(qr));
}

function enviarIngresoPermitidoAlBackoffice (qr){
  enviarIngresoAlBackoffice(qr, "Permitido");
}

function enviarIngresoDenegadoAlBackoffice (qr){
  enviarIngresoAlBackoffice(qr, "Denegado");
}

function enviarIngresoPermitidoConSintomasAlBackoffice (qr){
  enviarIngresoAlBackoffice(qr, "Permitido con síntomas");
}

function denegarAcceso (){
  let qr = document.querySelector('#appNavigator').topPage.data;
  enviarIngresoDenegadoAlBackoffice (qr);
  ocultarDDJJ();
}

function aceptarAcceso (){
  let qr = document.querySelector('#appNavigator').topPage.data;
  enviarIngresoPermitidoConSintomasAlBackoffice (qr);
  ocultarDDJJ();
}